# Changelog

## 2020-07-01 - v0.1.1

* Stock movements!

## 2020-07-01 - v0.1.0

* Only supports APIv1 for Products!
