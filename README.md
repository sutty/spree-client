# Spree Ecommerce API client

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'spree_client'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install spree_client

## Usage

Get an API key for your user in `/admin/users`.

```ruby
spree = SpreeClient::Api::V1.new api_key: 'api key', spree_url: 'http://localhost:3000', store: 'https://my.store'

spree.products.index
=> true

# The results are in the response
spree.products.response
=> []
```

[Read the documentation](https://rubydoc.info/gems/spree_client)


## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/spree_client>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the `spree_client` project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code
of conduct](https://sutty.nl/en/code-of-conduct/).
