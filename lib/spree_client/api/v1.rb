require 'httparty'
require_relative 'v1/resources'
require_relative 'v1/products'
require_relative 'v1/variants'
require_relative 'v1/stock_locations'
require_relative 'v1/stock_items'
require_relative 'v1/stock_movements'

module SpreeClient
  module API
    class V1
      include ::HTTParty

      attr_reader :api_key, :store

      def initialize(api_key:, spree_url: 'http://localhost:3000', store: nil)
        @api_key = api_key
        @store = HTTParty.normalize_base_uri(store) if store
        self.class.default_options[:base_uri] = HTTParty.normalize_base_uri(spree_url)
      end

      # TODO: Meta programming
      def products(**args)
        @products ||= {}
        @products[args.hash.to_s] ||= Products.new **{ api: self }.merge(args)
      end

      def variants(**args)
        @variants ||= {}
        @variants[args.hash.to_s] ||= Variants.new **{ api: self }.merge(args)
      end

      def stock_locations(**args)
        @stock_locations ||= {}
        @stock_locations[args.hash.to_s] ||= StockLocations.new **{ api: self }.merge(args)
      end

      def stock_movements(**args)
        @stock_movements ||= {}
        @stock_movements[args.hash.to_s] ||= StockMovements.new **{ api: self }.merge(args)
      end

      def stock_items(**args)
        @stock_items ||= {}
        @stock_items[args.hash.to_s] ||= StockItems.new **{ api: self }.merge(args)
      end

      def headers(extra = {})
        extra.merge({ 'Content-Type' => 'application/json', 'X-Spree-Token' => api_key, 'Origin' => store })
      end
    end
  end
end
