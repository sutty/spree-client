module SpreeClient
  module API
    class V1
      # Products
      # @see SpreeClient::API:V1::Resource
      class Products < Resources
        ENDPOINT = '/api/v1/products'
        RESOURCE = SpreeClient::Models::Product
        NAME     = 'product'

        # @return [SpreeClient::API::V1::Variants]
        def variants(**args)
          product_id = default_args.dig(:id) || args.dig(:product_id)

          raise ArgumentError, 'Needs a product ID' unless product_id

          @variants ||= {}
          @variants[product_id.to_s.to_sym] ||= Variants.new **{ api: api, product_id: product_id }.merge(args)
        end
      end
    end
  end
end
