class SpreeClient
  module API
    class V1
      # Properties
      # @see SpreeClient::API:V1::Resource
      class Properties < Resources
        ENDPOINT = '/api/v1/properties'
        RESOURCE = Property
      end
    end
  end
end
