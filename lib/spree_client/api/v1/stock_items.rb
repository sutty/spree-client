module SpreeClient
  module API
    class V1
      # Stock Movements
      # @see SpreeClient::API:V1::Resources
      class StockItems < Resources
        ENDPOINT = '/api/v1/stock_locations/:stock_location_id/stock_items'
        RESOURCE = SpreeClient::Models::StockItem
        NAME     = 'stock_item'

        def update(_); end
        def destroy(_); end

        # @return [SpreeClient::API::V1::StockMovements]
        def stock_movements(**args)
          stock_location_id = default_args.dig(:stock_location_id) || args.dig(:stock_location_id)
          stock_item_id = default_args.dig(:id) || args.dig(:stock_item_id)

          raise ArgumentError, 'Needs a stock location ID' unless stock_location_id

          @stock_movements ||= {}
          @stock_movements[stock_location_id.to_s.to_sym] ||= StockMovements.new **{ api: api, stock_location_id: stock_location_id, stock_item_id: stock_item_id }.merge(args)
        end
      end
    end
  end
end
