module SpreeClient
  module API
    class V1
      # Stock Locations
      # @see SpreeClient::API:V1::Resource
      class StockLocations < Resources
        ENDPOINT = '/api/v1/stock_locations'
        RESOURCE = SpreeClient::Models::StockLocation
        NAME     = 'stock_location'

        # @return [SpreeClient::API::V1::StockMovements]
        def stock_movements(**args)
          stock_location_id = default_args.dig(:id) || args.dig(:stock_location_id)

          raise ArgumentError, 'Needs a stock location ID' unless stock_location_id

          @stock_movements ||= {}
          @stock_movements[stock_location_id.to_s.to_sym] ||= StockMovements.new **{ api: api, stock_location_id: stock_location_id }.merge(args)
        end

        # @return [SpreeClient::API::V1::StockItems]
        def stock_items(**args)
          stock_location_id = default_args.dig(:id) || args.dig(:stock_location_id)

          raise ArgumentError, 'Needs a stock location ID' unless stock_location_id

          @stock_items ||= {}
          @stock_items[stock_location_id.to_s.to_sym] ||= StockItems.new **{ api: api, stock_location_id: stock_location_id }.merge(args)
        end
      end
    end
  end
end
