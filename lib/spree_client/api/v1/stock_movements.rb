module SpreeClient
  module API
    class V1
      # Stock Movements
      # @see SpreeClient::API:V1::Resources
      class StockMovements < Resources
        ENDPOINT = '/api/v1/stock_locations/:stock_location_id/stock_movements'
        RESOURCE = SpreeClient::Models::StockMovement
        NAME     = 'stock_movement'

        def update(_); end
        def destroy(_); end
      end
    end
  end
end
