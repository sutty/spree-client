class SpreeClient
  module API
    class V1
      # Taxonomies
      # @see SpreeClient::API:V1::Resource
      class Taxonomies < Resources
        ENDPOINT = '/api/v1/taxonomies'
        RESOURCE = Taxonomy
      end
    end
  end
end
