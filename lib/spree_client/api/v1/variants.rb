module SpreeClient
  module API
    class V1
      # Variants
      # @see SpreeClient::API:V1::Resource
      class Variants < Resources
        ENDPOINT = '/api/v1/variants'
        RESOURCE = SpreeClient::Models::Variant
        NAME     = 'variant'
      end
    end
  end
end
