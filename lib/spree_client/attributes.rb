module SpreeClient
  # @see Spree::PermittedAttributes
  class Attributes
    PRODUCT = %i[id name description available_on discontinue_on
                 permalink meta_description meta_keywords meta_title
                 price sku deleted_at prototype_id option_values_hash
                 weight height width depth shipping_category_id
                 tax_category_id cost_currency cost_price
                 option_type_ids taxon_ids pay_what_you_can
                 extra_attributes].freeze

    VARIANT = %i[id name presentation cost_price discontinue_on
                 lock_version position track_inventory product_id
                 product option_values_attributes price weight height
                 width depth sku cost_currency options
                 option_value_ids].freeze

    STOCK_LOCATION = %i[name active address1 address2 city zipcode
                        backorderable_default state_name state_id
                        country_id phone propagate_all_variants].freeze

    STOCK_ITEM = %i[stock_location_id variant stock_location backorderable variant_id].freeze

    STOCK_MOVEMENT = %i[id stock_location_id quantity stock_item
                        stock_item_id originator action].freeze

  end
end
