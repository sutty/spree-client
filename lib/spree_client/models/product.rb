module SpreeClient
  module Models
    # @see Spree::Product
    # @see SpreeClient::Attributes
    Product = Struct.new *Attributes::PRODUCT, keyword_init: true
  end
end
