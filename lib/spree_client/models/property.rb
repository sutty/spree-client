# @see Spree::Property
Property = Struct.new :id, :name, :presentation, keyword_init: true do
  def to_s
    'property'.freeze
  end
end
