module SpreeClient
  module Models
    # @see Spree::StockItem
    # @see SpreeClient::Attributes
    StockItem = Struct.new *Attributes::STOCK_ITEM, keyword_init: true
  end
end
