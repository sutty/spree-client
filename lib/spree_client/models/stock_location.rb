module SpreeClient
  module Models
    # @see Spree::StockLocation
    # @see SpreeClient::Attributes
    StockLocation = Struct.new *Attributes::STOCK_LOCATION, keyword_init: true
  end
end
