module SpreeClient
  module Models
    # @see Spree::StockMovement
    # @see SpreeClient::Attributes
    StockMovement = Struct.new *Attributes::STOCK_MOVEMENT, keyword_init: true
  end
end
