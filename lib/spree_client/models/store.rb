# Stores are different frontends for the same backend.
#
# @see {https://guides.spreecommerce.org/user/configuration/configuring_multi_stores.html}
Store = Struct.new :id, :name, :url,
                   :seo_title, :meta_description, :meta_keywords,
                   :mail_from_address, :default_currency,
                   :facebook, :twitter, :instagram, keyword_init: true
