Taxonomy = Struct.new :id, :name, keyword_init: true do
  def to_s
    'taxonomy'.freeze
  end
end
