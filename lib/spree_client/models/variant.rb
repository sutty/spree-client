module SpreeClient
  module Models
    # @see Spree::Variant
    # @see SpreeClient::Attributes
    Variant = Struct.new *Attributes::VARIANT, keyword_init: true
  end
end
